CXX = g++
BIN = rule110
SRCDIR = src
OBJDIR = build
SRCS = $(wildcard $(SRCDIR)/*.cpp)
HDRS = $(wildcard $(SRCDIR)/*.h)
OBJS = $(addprefix $(OBJDIR)/, $(patsubst $(SRCDIR)/%.cpp, %.o, $(SRCS)))
CXXFLAGS = -fPIC -g -std=c++11 -Wall -I$(SRCDIR) -lncursesw
VPATH=$(SRCDIR)

.SUFFIXES:
.SUFFIXES: .o .cpp

.PHONY: all clean run

all: $(BIN)

clean:
	rm -rf $(OBJDIR) $(BIN)

run: $(BIN)
	./$(BIN)

$(BIN): $(OBJS)
	$(CXX) -o $@ $^ $(CXXFLAGS)

$(OBJS): | $(OBJDIR)

$(OBJDIR)/%.o: %.cpp $(HDRS)
	$(CXX) -c -o $@ $< $(CXXFLAGS)

$(OBJDIR):
	mkdir $(OBJDIR)


