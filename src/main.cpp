/*******************************************************************************
 * Copyright (c) 2018, Kyle Robbertze
 *
 * This project is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU General Public License version 3 as 
 * published by the Free Software Foundation.
 *
 * It is distributed in the hope that it will be useful, but WITHOUT 
 * ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this project. If not, see <http://www.gnu.org/licenses/>.
 *
 *    Description:  Main entry point to the Rule 110 application
 * 
 *        Created:  01/11/2018 21:11:35
 *       Compiler:  g++
 * 
 *         Author:  Kyle Robbertze (kr), kyle@paddatrapper.com
 *******************************************************************************/
#include <bitset>
#include <chrono>
#include <cstdlib>
#include <iostream>
#include <ncurses.h>
#include <thread>
#include <vector>

#include "rule110.h"

int main() {
    setlocale(LC_ALL, "");
    initscr();
    noecho();
    curs_set(false);
    start_color();
    init_pair(1, COLOR_WHITE, COLOR_BLACK);

    Rule110::Rule110 app (163562719, 31);
    std::vector<int> buffer (LINES, 0);
    int head = 0;
    int generation = 0;
    while (true) {
        buffer[head] = app.getString();
        int length = app.getLength();
        clear();
        for (size_t i = 0; i < buffer.size(); ++i) {
            int iteration = buffer[(head + i) % buffer.size()];
            for (int j = 0; j < length; ++j) {
                int offset = length - j - 1;
                bool value = (iteration & (1 << offset)) >> offset;
                if (value) {
                    mvprintw(i - 1, j * 2, "\u25A0");
                }
            }
        }
        mvprintw(0, COLS - 4, "%d", generation);
        refresh();
        app.iterate();
        head = (head + 1) % buffer.size();
        ++generation;
        std::this_thread::sleep_for(std::chrono::milliseconds(100));
    }
    endwin();
    return EXIT_SUCCESS;
}
