/*******************************************************************************
 * Copyright (c) 2018, Kyle Robbertze
 *
 * This project is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU General Public License version 3 as 
 * published by the Free Software Foundation.
 *
 * It is distributed in the hope that it will be useful, but WITHOUT 
 * ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this project. If not, see <http://www.gnu.org/licenses/>.
 *
 *    Description:  Implementation of the Rule 110 application
 * 
 *        Created:  01/11/2018 21:28:56
 *       Compiler:  g++
 * 
 *         Author:  Kyle Robbertze (kr), kyle@paddatrapper.com
 *******************************************************************************/
#include "rule110.h"

namespace Rule110 {
    Rule110::Rule110() : Rule110(1, 14) {}

    Rule110::Rule110(long seed, int length) : string(seed), length(length) {}

    void Rule110::iterate() {
        long newIteration = 0;
        for (int i = 0; i < length; ++i) {
            char pattern = 0;
            if (i == 0) {
                pattern += (string & 1) << 2;
            } else {
                pattern += (string & (1 << (length - i))) >> (length - i) << 2;
            }
            pattern += (string & (1 << (length - i - 1))) >> (length - i - 1) << 1;
            if (i == length - 1) {
                pattern += (string & (1 << (length - 1))) >> (length - 1);
            } else {
                pattern += (string & (1 << (length - i - 2))) >> (length - i - 2);
            }
            switch (pattern) {
                case 0b110:
                    newIteration += 1 << (length - i - 1);
                    break;
                case 0b101:
                    newIteration += 1 << (length - i - 1);
                    break;
                case 0b011:
                    newIteration += 1 << (length - i - 1);
                    break;
                case 0b010:
                    newIteration += 1 << (length - i - 1);
                    break;
                case 0b001:
                    newIteration += 1 << (length - i - 1);
                    break;
            }
        }
        string = newIteration;
    }

    long Rule110::getString() {
        return string;
    }

    int Rule110::getLength() {
        return length;
    }
}
