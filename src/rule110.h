/*******************************************************************************
 * Copyright (c) 2018, Kyle Robbertze
 *
 * This project is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU General Public License version 3 as 
 * published by the Free Software Foundation.
 *
 * It is distributed in the hope that it will be useful, but WITHOUT 
 * ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this project. If not, see <http://www.gnu.org/licenses/>.
 *
 *    Description:  Rule 110 Game of Life
 * 
 *        Created:  01/11/2018 21:14:54
 *       Compiler:  g++
 * 
 *         Author:  Kyle Robbertze (kr), kyle@paddatrapper.com
 *******************************************************************************/
#ifndef _RULE_110_H
#define _RULE_110_H

namespace Rule110 {
    class Rule110 {
        public:
            Rule110();
            Rule110(long seed, int length);
            void iterate();
            long getString();
            int getLength();
        private:
            long string;
            int length;
    };
}
#endif
