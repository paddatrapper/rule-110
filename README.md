# Rule 110
The [Rule 110](https://en.wikipedia.org/wiki/Rule_110) cellular automaton is an
elementary cellular automaton and has been
[proven](https://wpmedia.wolfram.com/uploads/sites/13/2018/02/15-1-1.pdf) to be
Turing complete.

# Building
It uses the ncurses library to render the display. This can be installed using

    $ sudo apt install libncurses-dev

After it is installed, Rule 110 can be installed using make

    $ make
